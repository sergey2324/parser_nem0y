<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%post_intermediate}}`.
 */
class m190828_105233_create_post_intermediate_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('post_intermediate', [
            'id' => $this->primaryKey(),
            'text' => $this->string(),
            'image' => $this->string(),
            'video' => $this->string(),
            'tags' => $this->string(),
            'url_group' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('post_intermediate');
    }
}
