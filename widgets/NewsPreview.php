<?php
/**
 * Created by PhpStorm.
 * User: NEMOY
 * Date: 05.09.2019
 * Time: 20:53
 */

namespace app\widgets;


use app\models\tables\News;
use yii\base\Widget;

class NewsPreview extends Widget
{
    public $model;

    public function run()
    {
        if(is_a($this->model, News::class)){
            return $this->render('news_preview', [
                'model' => $this->model,
            ]);
        }
    }

}