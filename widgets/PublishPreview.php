<?php
/**
 * Created by PhpStorm.
 * User: NEMOY
 * Date: 05.09.2019
 * Time: 20:53
 */

namespace app\widgets;


use app\models\tables\News;
use app\models\tables\PostFinal;
use app\models\tables\TempAudio;
use app\models\tables\TempPhoto;
use app\models\tables\TempVideo;
use yii\base\Widget;
use yii\helpers\Html;

class PublishPreview extends Widget
{
    public $model;

    public function run(){
        if(is_a($this->model, PostFinal::class)){
            foreach ($this->getPhotoArray($this->model->id) as $one){
                    if ($one) {
                        $url = $one;
                }
            }
            return $this->render('publish_preview', [
                'model' => $this->model,
                'photo' => $url,
                'carouselItems' => $this->getItemsCarousel($this->model->id),
            ]);
        }
    }

    public function getItemsCarousel($id_item){
        $itemsCarousel = [];
        foreach ($this->getPhotoArray($id_item) as $one){
            $itemsCarousel[] = [
                'content' => Html::img($one),
                'controls' => [
                    Html::tag('i', '', ['class' => 'glyphicon glyphicon-chevron-left']),
                    Html::tag('i', '', ['class' => 'arrow-right']),
                ]
            ];
        }
        return $itemsCarousel;
    }

    private function getPhotoArray($id_item){
        $PhotoArray = [];
        foreach (TempPhoto::findAll(['id_post_intermediate' => $id_item]) as $one) {
            $once = $one->getAttributes($one->photoSizes);
            foreach ($once as $item) {
                if ($item) {
                    $url = $item;
                }
            }
            $PhotoArray[] = $url;
        }
       return  $PhotoArray;
    }

}