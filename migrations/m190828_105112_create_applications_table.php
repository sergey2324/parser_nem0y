<?php

use yii\db\Migration;

/**
 * Handles the creation of table `applications`.
 */
class m190828_105112_create_applications_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('applications', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'app_id' => $this->integer(),
            'secret_key' => $this->string(32),
            'proxy' => $this->string(),
            'network_id' => $this->smallInteger(6),
            'user_id' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('applications');
    }
}
