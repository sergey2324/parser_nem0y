<?php

use yii\db\Migration;

/**
 * Handles the creation of table `post_final`.
 */
class m190828_105248_create_post_final_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('post_final', [
            'id' => $this->primaryKey(),
            'text' => $this->string(),
            'image' => $this->string(),
            'video' => $this->string(),
            'tags' => $this->string(),
            'status' => $this->smallInteger(6),
            'id_user' =>$this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('post_final');
    }
}
