<?php
use yii\helpers\Url;
\app\assets\TaskAsset::register($this);
/** @var $model \app\models\Applications */
?>

<div class="task-container">
    <div class="task-preview id_<?=$model['id']?>">
        <div class="task-preview-header name"><?= $model->name ?></div>
        <div class="task-preview-content app_id"><?=$model['app_id']?></div>
        <div class="task-preview-content network_id"><?=$model['network_id']?></div>
        <div class="task-preview-content user_id"><?=$model['user_id']?></div>
        <div class="task-preview-content proxy"><?=$model['proxy']?></div>
        <div class="task-preview-content secret_key"><?=$model['secret_key']?></div>
        <button class="btn btn-danger deleteShow" data="<?=$model['id']?>" id="1" title="Удалить">×</button>
        <button type="button" data="<?=$model['id']?>" name = "<?=$model['name']?>" app_id = "<?=$model['app_id']?>" network_id = "<?=$model['network_id']?>"
                user_id = "<?=$model['user_id']?>" proxy = "<?=$model['proxy']?>" secret_key = "<?=$model['secret_key']?>"
                class="glyphicon glyphicon-pencil editShow"></button>
    </div>
</div>




