<?php

namespace app\controllers;

use app\models\Applications;
use app\models\filters\AppSearch;
use Yii;

class ApplicationsController extends \yii\web\Controller
{

    public function actionIndex()
    {
        $model = new Applications();
        $searchModel = new AppSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        if ($model->load(\Yii::$app->request->post())) {
            if ($model->validate()) {
                $model->save();
                return $this->redirect(['/applications']);
            }
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);

    }

    public function actionRemove()
    {
        if(\Yii::$app->request->isAjax){
            $id = Yii::$app->request->post('id');
            $model = Applications::find()->where(['id'=>$id])->one();
            $model->delete();
            return json_encode(['idDelete'=>$id]);
        }
    }
}
