<?php

use yii\db\Migration;

/**
 * Handles the creation of table `photo`.
 */
class m190903_122146_create_photo_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('temp_photo', [
            'id' => $this->primaryKey(),
            'id_post_intermediate' => $this->integer(),
            'photo_75' => $this->string(),
            'photo_130' => $this->string(),
            'photo_604' => $this->string(),
            'photo_807' => $this->string(),
            'photo_1280' => $this->string(),
            'photo_2560' => $this->string(),
            'width' => $this->integer(),
            'height' => $this->integer(),
            'text' => $this->string(),

        ]);
        $this->createTable('media_content_photo', [
            'id' => $this->primaryKey(),
            'id_post_final' => $this->integer(),
            'photo_75' => $this->string(),
            'photo_130' => $this->string(),
            'photo_604' => $this->string(),
            'photo_807' => $this->string(),
            'photo_1280' => $this->string(),
            'photo_2560' => $this->string(),
            'width' => $this->integer(),
            'height' => $this->integer(),
            'text' => $this->string(),

        ]);

        $this->createIndex("temp_photo_id_post_idx", 'temp_photo', ['id_post_intermediate']);
        $this->createIndex("media_content_photo_id_post_idx", 'media_content_photo', ['id_post_final']);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('temp_photo');
        $this->dropTable('media_content_photo');
    }
}
