<?php
/**
 * 29.08.2019
 * 20:29
 */

namespace app\models;

use app\models\tables\PostIntermediate;
use app\models\tables\TempPhoto;
use yii\base\Model;


class Source extends Model
{
    public $groupID;
    public $maxNumber = 100;
    private $serviceKey = 'b69e2d38b69e2d38b69e2d38c2b6f2b5cabb69eb69e2d38ebf62b629c1ad0e78c77cedd';
    static public $postsCount = 0;
    static public $repostsCount = 0;
    static public $alreadyExists = 0;
    static public $notSaved = 0;


    public function generatePostsNumber()
    {
        $maxNumber = $this->maxNumber;
        $minNumber = 10;
        $arrValues[0] = ['Выберите количество публикаций'];

        while ($minNumber <= $maxNumber) {
            $arrValues[$minNumber] = $minNumber;
            $minNumber += 10;

        }
        return $arrValues;
    }

    public function getGroupIdByUrl($url)
    {
        $separatedUrl = explode('/', $url);
        $shortName = end($separatedUrl);

        $params = [
            'group_ids' => $shortName,
            'access_token' => $this->serviceKey,
            'v' => '5.71'
        ];
        $url = 'https://api.vk.com/method/groups.getById?' . http_build_query($params);
        $response = json_decode(file_get_contents($url), true);

        //TODO throw Exception if url is incorrect

        return '-' . $response['response'][0]['id'];
    }

    public function getPosts($groupUrl, $postsNumber = 100)
    {

        $post = \Yii::$app->request->post();


        if (empty($post['groupUrl']) || empty($post['selector'])) {
            //TODO throw an Exception
            return 'Некоторые поля были пустыми';
        }

        $groupID = $this->getGroupIdByUrl($groupUrl);

        $params = [
            'owner_id' => $groupID,
            'offset' => '0',
            'count' => $postsNumber,
            'filter' => 'all',
            'access_token' => $this->serviceKey,
            'v' => '5.71'
        ];
        $url = 'https://api.vk.com/method/wall.get?' . http_build_query($params);

        //TODO: add an Exeption hanling vk errors in response
        $response = json_decode(file_get_contents($url), true);

        $message = $this->savePostsToDb($response, $groupID, $groupUrl);

        return $message;

    }

    public function savePostsToDb($postsArray, $groupID, $groupUrl)
    {
        $posts = $postsArray['response']['items'];


        foreach ($posts as $key => $onePost) {

            if (isset($onePost['copy_history'])) {

                static::$repostsCount++;
                //it's a repost
                //TODO develop method if we need to save reposts too
                continue;
            }

            //outdated method when table get clear after submitting
            //TODO: delete it in next release
            /*if (PostIntermediate::find()
                ->where(['id_vk' => $onePost['id'], 'owner_id' => $onePost['owner_id']])
                ->exists()) {
                static::$alreadyExists++;
                continue;
            }*/

            $model = new PostIntermediate();

            $model->text = $onePost['text'];
            $model->id_group = $groupID;
            $model->url_group = $groupUrl;
            $model->id_vk = $onePost['id'];
            $model->owner_id = $onePost['owner_id'];
            $model->date = $onePost['date'];
            $model->marked_as_ads = $onePost['marked_as_ads'];
            $model->post_type = $onePost['post_type'];


            if ($model->save(false)) {
                static::$postsCount++;

            } else {
                //TODO throw an Exception
                static::$notSaved++;

            }

            if (isset($onePost['attachments'])) {


                foreach ($onePost['attachments'] as $attachment) {

                    if ($attachment['type'] == 'photo') {

                        /*var_dump($attachment);
                        die;*/

                        $imageProps = $attachment['photo'];
                        $photo = new TempPhoto();
                        $photo->id_post_intermediate = $model->id;

                        if (!$this->savePhotos($photo, $imageProps)) {
                            //TODO throw an Exception
                            return 'Some photos from post\'s haven\'t been saved.';
                        }

                    }

                    if ($attachment['type'] == 'video') {
                        //TODO save video when access will change

                    }
                }
            }

        }

        return static::$postsCount;
    }


    public function savePhotos(TempPhoto $tempPhoto, $imageProps)
    {
        $tableColumns = array_keys($tempPhoto->attributes);
        $tableColumnsNoId = array_diff($tableColumns, ['id']);

        foreach ($imageProps as $key => $value) {

            foreach ($tableColumnsNoId as $prop) {

                if ($key == $prop) {
                    $tempPhoto[$prop] = $value;
                }
            }
        }

        if ($tempPhoto->save(false)) {
            return true;
        } else {
            return false;
        }
    }
}