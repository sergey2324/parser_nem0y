<?php

use yii\db\Migration;

/**
 * Class m190903_121351_correction_post_archive_table
 */
class m190903_121351_correction_post_archive_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('post_archive', 'id_vk', 'string');
        $this->addColumn('post_archive', 'owner_id', 'string');
        $this->addColumn('post_archive', 'date', 'string');
        $this->addColumn('post_archive', 'marked_as_ads', 'integer');
        $this->addColumn('post_archive', 'post_type', 'string');
        $this->addColumn('post_archive', 'id_group', 'string');
        $this->dropColumn('post_archive', 'image');
        $this->dropColumn('post_archive', 'video');
        $this->db->createCommand("ALTER TABLE {{%post_archive}} MODIFY [[text]] longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci")->execute();
        $this->db->createCommand("ALTER TABLE {{%post_archive}}  CHANGE `id` `id` INT(11) NOT NULL")->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('post_archive', 'id_vk');
        $this->dropColumn('post_archive', 'owner_id');
        $this->dropColumn('post_archive', 'date');
        $this->dropColumn('post_archive', 'marked_as_ads');
        $this->dropColumn('post_archive', 'post_type');
        $this->dropColumn('post_archive', 'id_group');
        $this->addColumn('post_archive', 'image','string');
        $this->addColumn('post_archive', 'video','string');
        $this->db->createCommand("ALTER TABLE {{%post_archive}}  CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT")->execute();
    }

}
