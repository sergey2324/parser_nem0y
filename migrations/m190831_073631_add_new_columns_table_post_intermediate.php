<?php

use yii\db\Migration;

/**
 * Class m190831_073631_add_new_columns_table_post_intermediate
 */
class m190831_073631_add_new_columns_table_post_intermediate extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('post_intermediate', 'id_vk', 'string');
        $this->addColumn('post_intermediate', 'owner_id', 'string');
        $this->addColumn('post_intermediate', 'date', 'string');
        $this->addColumn('post_intermediate', 'marked_as_ads', 'integer');
        $this->addColumn('post_intermediate', 'post_type', 'string');
        $this->addColumn('post_intermediate', 'id_group', 'string');
        $this->alterColumn('post_intermediate', 'text', 'longtext');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('post_intermediate', 'id_vk');
        $this->dropColumn('post_intermediate', 'owner_id');
        $this->dropColumn('post_intermediate', 'date');
        $this->dropColumn('post_intermediate', 'marked_as_ads');
        $this->dropColumn('post_intermediate', 'post_type');
        $this->dropColumn('post_intermediate', 'id_group');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190831_073631_add_new_columns_table_post_intermediate cannot be reverted.\n";

        return false;
    }
    */
}
