<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $buttonName string */
/* @var $registration bool */
?>
<div class = 'site-signup'>

    <p> Пожалуйста, заполните эти поля: </p>

    <div class = 'row'>
        <div class = 'col-lg-5'>
            <?php $form = \yii\bootstrap\ActiveForm::begin([
                    'id' => 'form-signup',
                    'options' => ['enctype' => 'multipart/form-data'],
                    'layout' => 'horizontal',
                    'fieldConfig' => ['horizontalCssClasses' =>
                    ['label' => 'col-sm-2',]],
            ]); ?>

            <?= $form->field($model, 'username')->textInput() ?>

            <?= $form->field($model, 'avatar')->fileInput() ?>

            <?= $form->field($model, 'email') ?>

            <? if($registration) {?>
                <?= $form->field($model, 'password')->passwordInput()?>
            <? } ?>

            <?= Html::submitButton($buttonName, ['class' => 'btn btn-primary',
                'name' => 'signup-button']) ?>

            <?php \yii\bootstrap\ActiveForm::end(); ?>
        </div>
    </div>
</div>