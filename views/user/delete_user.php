<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $title string */
/* @var $user_id int */
//$model->id =  Yii::$app->user->identity->id;
?>
<div class="user-view">

    <h3 class = 'text-center'><?= $title ?></h3>
    <br>
    <h4> Нам очень жаль, что вы хотите удалить свой аккаунт. Как только вы подтвердите свое желание,
        все ваши данные будут безвозвратно удалены без возможности восстановления.
    </h4>
    <br>
    <p class = 'text-center'>
        <?= Html::a('Удалить', ['delete_user', 'id' => $user_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

</div>