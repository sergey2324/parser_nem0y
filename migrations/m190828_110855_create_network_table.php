<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%network}}`.
 */
class m190828_110855_create_network_table extends Migration
{
    protected $tableName = 'network';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'name' => $this->string(50)
        ]);

        $this->batchInsert($this->tableName, ['name'], [
            ['VK'],
            ['Одноклассники'],
            ['Instagram'],
            ['Telegram'],
            ['Facebook'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('network');
    }
}
