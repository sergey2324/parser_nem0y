<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $title string */
//$model->id =  Yii::$app->user->identity->id;
?>
<div class="user-view text-center">

    <h3><?= $title ?></h3>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'username',
            ['label' => 'Аватар',
                'value' => ('/upload/user/' . $model->avatar),
                'format' => ['image',['height' => '100']],
            ],
            'email',
        ],
    ]) ?>

</div>