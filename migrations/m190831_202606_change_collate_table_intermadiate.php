<?php

use yii\db\Migration;

/**
 * Class m190831_202606_change_collate_table_intermadiate
 */
class m190831_202606_change_collate_table_intermadiate extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->db->createCommand("ALTER TABLE {{%post_intermediate}} MODIFY [[text]] longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci")->execute();

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190831_202606_change_collate_table_intermadiate cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190831_202606_change_collate_table_intermadiate cannot be reverted.\n";

        return false;
    }
    */
}
