<?php


namespace app\assets;
use yii\web\AssetBundle;


class TaskAsset extends AssetBundle
{

    public $js = [
        'js/task.js',
    ];


}