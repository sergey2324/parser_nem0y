<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div class="applications-index">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name') ?>
        <?= $form->field($model, 'app_id') ?>
        <?= $form->field($model, 'network_id') ?>
        <?= $form->field($model, 'user_id') ?>
        <?= $form->field($model, 'proxy') ?>
        <?= $form->field($model, 'secret_key') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- applications-index -->

<div class="tasks-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    echo \yii\widgets\ListView::widget([
        'itemView'=>'myview',
        'dataProvider'=>$dataProvider,
        'viewParams'=>[
            'hide'=>'true'
        ],
        'options' => [
            'tag' => 'div',
            'class' => 'preview-container',
            'id' => 'news-list',
        ],
        'layout' => "\n{items}",

    ]);

    ?>
