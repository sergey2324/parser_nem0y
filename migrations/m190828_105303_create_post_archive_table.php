<?php

use yii\db\Migration;

/**
 * Handles the creation of table `post_archive`.
 */
class m190828_105303_create_post_archive_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('post_archive', [
            'id' => $this->primaryKey(),
            'text' => $this->string(),
            'image' => $this->string(),
            'video' => $this->string(),
            'tags' => $this->string(),
            'status' => $this->smallInteger(),
            'size_content' => $this->integer(),
            'id_user' =>$this->integer()

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('post_archive');
    }
}
