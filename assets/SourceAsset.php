<?php
/**
 * 05.09.2019
 * 21:57
 */

namespace app\assets;


use yii\web\AssetBundle;

class SourceAsset extends AssetBundle
{
    /*public $basePath = '@webroot';
    public $baseUrl = '@web';*/
    public $css = [

    ];
    public $js = [
        'js/source.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];


}